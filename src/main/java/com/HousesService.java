package com;

import com.project.marco.resourcesOnServerRest.*;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.ArrayList;

@Path("houses")
public class HousesService {

    @GET
    @Produces({"application/json", "application/xml"})
    public Response getAllList(){ // raggiungo questo metodo solo con GET "http://localhost:1337/users"
        return Response.ok(Houses.getInstance()).build();
    }

    @Path("addHouse")
    @POST
    @Produces({"application/json", "application/xml"})
    public Response addHouse(House newHouse){ // GET "http://localhost:1337/houses/addHouse"
        ArrayList<House> listHouse = Houses.getInstance().addHouse(newHouse);
        if(listHouse!=null)
            return Response.ok(listHouse).build();
        else
            return Response.status(Response.Status.NOT_ACCEPTABLE).build(); // restituisco 406 se la casa è già presente
    }

    /* Esempio di input
    {
      "id": "45",
      "ip": "193.XXX.XXX.XXX",
      "port": "888"
    }
     */

    @Path("getHouses")
    @GET
    @Produces({"application/json", "application/xml"})
    public Response getHousesList(){
        return Response.ok(Houses.getInstance().getHouseslist()).build();
    }

    @Path("/remove/{id}")
    @DELETE
    public Response deleteHouseById(@PathParam("id") int id){
        Houses.getInstance().remove(id);
        return Response.status(Response.Status.ACCEPTED).build();
    }

    @Path("addGlobalStat")
    @POST
    @Consumes({"application/json", "application/xml"})
    public Response addGlobalStat(GlobalStat g){
        Houses.getInstance().addGlobalStat(g);
        return Response.ok().build();
    }

    /*
    {
        "value": "20",
        "timestamp": "12345"
    }
     */

    @Path("getGlobalStat")
    @GET
    @Produces({"application/json", "application/xml"})
    public Response getGlobalStatList(){
        return Response.ok(Houses.getInstance().getGlobalStatList()).build();
    }

    @Path("addLocalStat")
    @POST
    @Consumes({"application/json", "application/xml"})
    public Response addLocallStat(LocalStat l){
        Houses.getInstance().addLocallStat(l);
        return Response.ok().build();
    }

    /*
    {
        "id": "45",
        "value": "20",
        "timestamp": "12345"
    }
     */

    @Path("getLocalStat")
    @GET
    @Produces({"application/json", "application/xml"})
    public Response getLocalStatList(){
        return Response.ok(Houses.getInstance().getLocalStatList()).build();
    }
}
package com.project.marco.globalStatistic;

import com.project.marco.resourcesOnServerRest.GlobalStat;
import com.project.marco.resourcesOnServerRest.LocalStat;

import java.util.ArrayList;
import java.util.Arrays;

public class GlobalStatisticManager {

    private ArrayList<Integer> idList;
    private ArrayList<LocalStat> localStatsList;

    public GlobalStatisticManager(ArrayList<Integer> idList){
        this.idList = idList;
        this.localStatsList = new ArrayList<>();
    }

    public void addId(int id){
        idList.add(id);
    }

    public void removeId(int id){
        idList.removeAll(Arrays.asList(id));
    }

    public void showAllId(){
        for(int id: idList)
            System.out.println("Ora la lista contiene: " + id);
    }

    public void addLocalStat(LocalStat localStat){
        localStatsList.add(localStat);
    }

    public GlobalStat calcGlobalStatIfPossible() {
        GlobalStat newGlobaStat = null;
        boolean canComputeGlobal = true;

        ArrayList<LocalStat> workingStatList = new ArrayList<>(localStatsList);
        ArrayList<LocalStat> localStatForGlobal = new ArrayList<>();

        for(int id: idList){ // ESTERNAMENTE ITERO SUGLI ID
            LocalStat localStatForId = null;

            for(LocalStat locSt: workingStatList){ // INTERNAMENTE ITERO SULLE STATISTICHE LOCALI SULLA LISTA DI LAVORO
                if (locSt.getId() == id){
                    localStatForId = locSt;
                    break;
                }
            }

            if (localStatForId != null){ // SE HO TROVATO UNA STATISTICA LOCALE PER QUELL'ID ALLORA LA RIMUOVO DALLA LISTA DI LAVORO E LA AGGIUNGO ALLA LISTA PER CALOOLARE LA STATISTICA GLOBALE
                workingStatList.remove(localStatForId);
                localStatForGlobal.add(localStatForId);
            }
            else{ // ALTRIMENTI E' SUFFICIENTE CHE ANCHE 1 SOLA STATISTICA LOCALE NON SIA PRESENTE PER NON POTER CALCOLARE QUELLA GLOBALE
                canComputeGlobal = false;

                /*System.out.println("non ho abbastanza dati per generare la global stat");
                showAllId();*/

                break;
            }
        }

        if (canComputeGlobal){ // SE QUESTA VARIABILE NON E' MAI STATA MESSA A FALSE
            // CALCOLO LA STATISTICA GLOBALE USANDO LE STATISTICHE DENTRO localStatForGlobal
            newGlobaStat = calcGlobalStatistic(localStatForGlobal);

            // AGGIORNO LA LISTA DELLE STATISTICHE LOCALI SOSTITUIENDOLA CON QUELLA A CUI SONO STATI SOTTRATTE LE STATISTICHE PER COMPUTARE QUELLA GLOBALE CIOE' workingStatList
            localStatsList = workingStatList;
        }

        return newGlobaStat;
    }

    private GlobalStat calcGlobalStatistic(ArrayList<LocalStat> localStatForGlobal) {
        double sumValue = 0;
        long lastTime = 0;

        for(LocalStat locSt: localStatForGlobal){
            sumValue = sumValue + locSt.getValue();

            if (locSt.getTimestamp() > lastTime)
                lastTime = locSt.getTimestamp();
        }

        return new GlobalStat(sumValue, lastTime);
    }

    public void updateIdLIst(ArrayList<Integer> newIdList){
        this.idList = newIdList;
    }
}

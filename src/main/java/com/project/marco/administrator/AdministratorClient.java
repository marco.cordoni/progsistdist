package com.project.marco.administrator;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import com.project.marco.resourcesOnServerRest.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class AdministratorClient {  // http://localhost:1337/houses
    public static void main(String[] args) {

        boolean isActive = true;

        try {
            while (isActive){
                System.out.println("\n" +
                        "-------------------------------------------------------------------------------------------------\n" +
                        "Digit the number of the function required: \n\n" +
                        "   1) Show all houses\n" +
                        "   2) Show last N statistics for a specific house\n" +
                        "   3) Show last N global statistics\n" +
                        "   4) Show mean and standard deviation of the last N statistics of a specific house\n" +
                        "   5) Show mean and standard deviation of the last N global staistics\n" +
                        "   exit) Exit from the program\n" +
                        "-------------------------------------------------------------------------------------------------");

                String func = getFunction();

                System.out.println();

                if(func.equals("1")) // LISTA DELLE CASE
                    showAllHouses();

                else if(func.equals("2")) //ULTIME N STATISTICHE PER UNA SPECIFICA CASA
                    lastStatisticsForAnHouse();

                else if(func.equals("3")) // ULTIME N STATISTICHE GLOBALI
                    lastGlobalStatistics();

                else if(func.equals("4")) // DEVIAZIONE STANDARD E MEDIA DELLE ULTIME N STATISTICHE PER UNA SPECIFICA CASA
                    sdDevAndMeanForAnHouse();

                else if(func.equals("5")) // DEVIAZIONE STANDARD E MEDIA DELLE ULTIME N STATISTICHE GLOBALI
                    sdDevAndMeanForGlobalStat();

                else if(func.equals("exit")){
                    System.out.println("Exit from the program.....");
                    isActive = false;
                }
                else
                    System.out.println("The command " + func + " doesn't exist!");
            }
        }
        catch (ConnectException e){
            System.out.println("Connection refuse, server not active!");
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("Goodbye!!");
    }

    private static void sdDevAndMeanForGlobalStat() throws IOException, JSONException {
        ArrayList<Double> misurations;
        ArrayList<GlobalStat> globalStatsList;
        BufferedReader readInput = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("Write the number of last global misurations to use to calc mean and standard deviation: ");
        int lastN = getPositiveNumber(readInput);

        System.out.println();

        misurations = new ArrayList<>();
        globalStatsList = getLastNGlobalStats(lastN);

        System.out.println("Mean and standard deviation for the last " + lastN + " global misurations: ");

        if(globalStatsList.size() == 0)
            System.out.println("    No statistics found!");
        else {
            for (GlobalStat g : globalStatsList) {
                System.out.println("    " + g.toString());
                misurations.add(g.getValue());
            }

            // MEDIA E DEVIAZIONE STANDARD DELLE ULTIME N STATISTICHE GLOBALI
            Double[] misArray = new Double[misurations.size()];
            misArray = misurations.toArray(misArray);

            System.out.println("\nStandard deviations = " + calculateSD(misArray) + ", mean = " + mean(misArray));
        }
    }

    private static void sdDevAndMeanForAnHouse() throws IOException, JSONException {
        ArrayList<Double> misurations;
        ArrayList<LocalStat> localStatsList;
        BufferedReader readInput = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Write the id of the house: ");
        int id = getPositiveNumber(readInput);

        System.out.print("Write the number of last misurations to calculate mean and standard deviation: ");
        int lastN = getPositiveNumber(readInput);

        System.out.println();

        misurations = new ArrayList<>();
        localStatsList = getLastNLocalStatsForHouse(id, lastN);

        System.out.println("Mean and standard deviation for the last " + lastN + " misurations for the house with id = " + id +":");

        if(localStatsList.size() == 0)
            System.out.println("    No statistics found!");
        else {
            for (LocalStat l : localStatsList) {
                System.out.println("    " + l.toString());
                misurations.add(l.getValue());
            }
            // MEDIA E DEVIAZIONE STANDARD DELLE ULTIME N STATISTICHE DELLA CASA I
            Double[] misArray = new Double[misurations.size()];
            misArray = misurations.toArray(misArray);

            System.out.println("\nStandard deviations = " + calculateSD(misArray) + ", mean = " + mean(misArray));
        }
    }

    private static void lastGlobalStatistics() throws IOException, JSONException {
        ArrayList<GlobalStat> globalStatsList;
        BufferedReader readInput = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Write the number of last global misurations to see: ");
        int lastN = getPositiveNumber(readInput);
        globalStatsList = getLastNGlobalStats(lastN);

        System.out.println();

        System.out.println("Last " + lastN + " global misurations");

        if(globalStatsList.size() == 0)
            System.out.println("    No statistics found!");
        for (GlobalStat g : globalStatsList)
            System.out.println("    " + g.toString());
    }

    private static void lastStatisticsForAnHouse() throws IOException, JSONException {
        ArrayList<LocalStat> localStatsList;
        BufferedReader readInput = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("Write the id of the house: ");
        int id = getPositiveNumber(readInput);

        System.out.print("Write the number of last misurations to see: ");
        int lastN = getPositiveNumber(readInput);

        System.out.println();

        localStatsList = getLastNLocalStatsForHouse(id, lastN);

        System.out.println("Last " + lastN + " misurations for the house with id = " + id + ":");

        if(localStatsList.size() == 0)
            System.out.println("    No statistics found!");
        for (LocalStat l : localStatsList)
            System.out.println("    " + l.toString());
    }

    private static String getFunction() throws IOException {
        BufferedReader readFunction = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Function number: ");
        return readFunction.readLine();
    }

    private static void showAllHouses() throws IOException, JSONException {
        ArrayList<House> housesList = getHouses();
        System.out.println("All the houses: ");

        if(housesList.size() == 0)
            System.out.println("    No house found!");
        for (House h : housesList)
            System.out.println("    " + h.toString());
    }

    private static ArrayList<House> getHouses() throws IOException, JSONException {
        String urlToGet = "http://localhost:1337/houses/getHouses";
        HttpURLConnection conn = getConnection(urlToGet);
        JSONArray array = getJsonArray(conn);

        ArrayList<House> housesList = new ArrayList<>();

        for (int i = 0; i < array.length(); i++) {
            JSONObject object = array.getJSONObject(i);

            int id = Integer.parseInt(object.getString("id"));
            InetAddress ip = InetAddress.getByName(object.getString("ip"));
            int port = Integer.parseInt(object.getString("port"));

            House h = new House(id, ip, port);
            housesList.add(h);
        }
        conn.disconnect();
        return housesList;
    }

    private static ArrayList<LocalStat> getLastNLocalStatsForHouse(int identifier, int lastNMisurations) throws IOException, JSONException {
        String urlToGet = "http://localhost:1337/houses/getLocalStat";
        HttpURLConnection conn = getConnection(urlToGet);
        JSONArray array = getJsonArray(conn);

        ArrayList<LocalStat> localStatList = new ArrayList<LocalStat>();
        ArrayList<LocalStat> localStatListForId = new ArrayList<LocalStat>();

        for (int i = 0; i < array.length(); i++) {
            JSONObject object = array.getJSONObject(i);

            int id = Integer.parseInt(object.getString("id"));
            double value = Double.parseDouble(object.getString("value"));
            long timestamp = Long.parseLong(object.getString("timestamp"));

            LocalStat l = new LocalStat(id, value, timestamp);
            localStatList.add(l);
        }

        for (LocalStat h : localStatList)
            if (h.getId() == identifier)
                localStatListForId.add(h);

        if(lastNMisurations > localStatListForId.size())
            lastNMisurations = localStatListForId.size();

        List<LocalStat> lastPos = localStatListForId.subList(localStatListForId.size() - lastNMisurations, localStatListForId.size());

        conn.disconnect();

        return new ArrayList<LocalStat>(lastPos);
    }

    private static ArrayList<GlobalStat> getLastNGlobalStats(int lastNMisurations) throws IOException, JSONException {
        String urlToGet = "http://localhost:1337/houses/getGlobalStat";
        HttpURLConnection conn = getConnection(urlToGet);
        JSONArray array = getJsonArray(conn);

        ArrayList<GlobalStat> globalStatList = new ArrayList<GlobalStat>();

        for (int i = 0; i < array.length(); i++) {
            JSONObject object = array.getJSONObject(i);

            double value = Double.parseDouble(object.getString("value"));
            long timestamp = Long.parseLong(object.getString("timestamp"));

            GlobalStat g = new GlobalStat(value, timestamp);
            globalStatList.add(g);
        }

        if (lastNMisurations > globalStatList.size())
            lastNMisurations = globalStatList.size();

        List<GlobalStat> lastPos = globalStatList.subList(globalStatList.size() - lastNMisurations, globalStatList.size());

        conn.disconnect();

        return new ArrayList<GlobalStat>(lastPos);
    }

    private static HttpURLConnection getConnection(String urlToGet) throws IOException {
        URL url = new URL(urlToGet);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Accept", "application/json");

        if (conn.getResponseCode() != 200)
            throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
        return conn;
    }

    private static JSONArray getJsonArray(HttpURLConnection conn) throws IOException, JSONException {
        BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
        String output = br.readLine();
        return new JSONArray(output);
    }

    private static double calculateSD(Double[] numArray) {
        double sum = 0.0, standardDeviation = 0.0;
        int length = numArray.length;

        for(Double num : numArray) {
            sum += num;
        }

        Double mean = sum/length;

        for(Double num: numArray) {
            standardDeviation += Math.pow(num - mean, 2);
        }

        return Math.sqrt(standardDeviation/length);
    }

    private static double mean(Double[] m) {
        Double sum = 0.0;
        for (int i = 0; i < m.length; i++) {
            sum += m[i];
        }
        return sum / m.length;
    }

    private static int getPositiveNumber(BufferedReader readInput) throws IOException {
        int lastN;
        while (true){
            try {
                lastN = Integer.parseInt(readInput.readLine());

                if (lastN > 0)
                    break;
                else
                    throw new NumberFormatException();
            }
            catch (NumberFormatException e){
                System.out.println("Please write a number > 0");
            }
        }
        return lastN;
    }
}

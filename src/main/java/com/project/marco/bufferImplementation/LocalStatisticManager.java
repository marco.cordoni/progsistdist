package com.project.marco.bufferImplementation;

import com.project.marco.houseInTheRing.Queue;
import com.project.marco.resourcesOnServerRest.LocalStat;
import com.project.marco.simulation_src_2019.*;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import it.ewlab.ringMsg.RingMsgOuterClass.*;

import java.io.IOException;
import java.net.ConnectException;
import java.net.Socket;
import java.net.UnknownHostException;

public class LocalStatisticManager extends Thread {

    private int windowSize = 24;
    private double percOverlap = 0.5;
    private boolean isActive;
    public ThreadForBoost threadForBoost;

    private BufferImpl bufferImpl;
    private SmartMeterSimulator smartMeterSimulator;
    private Queue queue;

    public LocalStatisticManager(int idHouse, Queue queue) {
        this.bufferImpl = new BufferImpl(windowSize, percOverlap, idHouse);
        this.smartMeterSimulator = new SmartMeterSimulator(bufferImpl);
        this.isActive = true;
        this.queue = queue;
    }

    public void run() {
        smartMeterSimulator.start();

        while (isActive){
            LocalStat l = bufferImpl.getLocalStatistic();

            if (l != null){
                //System.out.println(l.toString()); // INVECE DI STAMPARLO LO INVIO A SERVER E SUCCESSORE, IL SUCCESSORE VA MODIFICATO SE CAMBIA NELLA RETE
                sendLocalStatToServer(l);
                putMsgInTheQueue(l);
            }
        }
    }

    private void putMsgInTheQueue(LocalStat localStat) {
        RingMsg msgLocalStat = RingMsg.newBuilder()
                .setTypeOfMsg(MsgType.LOCALSTAT)
                .setLocalStat(LocalStatProto.newBuilder()
                        .setId(localStat.getId())
                        .setValue(localStat.getValue())
                        .setTimestamp(localStat.getTimestamp()))
                .build();
        queue.put(msgLocalStat);
    }

    public void stopLocalStatisticManager(){
        isActive = false;
        smartMeterSimulator.stopMeGently();
        bufferImpl.forceWakeUp();
    }

    public void boost(RingMsg msg) {
        threadForBoost = new ThreadForBoost(smartMeterSimulator, queue, msg);
        threadForBoost.start();
    }

    private static void sendLocalStatToServer(LocalStat localStat) {
        Client client = Client.create();
        WebResource webResource = client.resource("http://localhost:1337/houses/addLocalStat");

        String toServer =
                "{\n" +
                        "  \"id\": \""        + localStat.getId()        + "\",\n" + // int
                        "  \"value\": \""     + localStat.getValue()     + "\",\n" + // double
                        "  \"timestamp\": \"" + localStat.getTimestamp() + "\"\n"  + // long
                "}";

        // INVIO STATISTICHE LOCALI AL SERVER
        try{
            ClientResponse response = webResource.type("application/json").post(ClientResponse.class, toServer);

            if (response.getStatus() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
            }
        }
        catch (ClientHandlerException e){
            System.out.println("Il server non è attivo e quindi non ho potuto inviare le statistiche locali");
        }

    }
}
package com.project.marco.bufferImplementation;

import com.project.marco.resourcesOnServerRest.LocalStat;
import com.project.marco.simulation_src_2019.Buffer;
import com.project.marco.simulation_src_2019.Measurement;

import java.util.ArrayList;

public class BufferImpl extends ArrayList<Measurement> implements Buffer {

    private ArrayList<Measurement> measureList;
    private int windowSize;
    private int overlap;
    private int idHouse;

    public BufferImpl(int windowSize, double percOverlap, int idHouse){
        measureList = new ArrayList<>();
        this.windowSize = windowSize;
        this.overlap = (int)(windowSize * percOverlap);
        this.idHouse = idHouse;
    }

    @Override
    public synchronized void addMeasurement(Measurement m) {
        measureList.add(m);
        if(measureList.size() >= windowSize)
            notify();
    }

    public synchronized LocalStat getLocalStatistic(){
        if (measureList.size() < windowSize) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        try{
            ArrayList<Measurement> window = new ArrayList<>(measureList.subList(0 , windowSize));
            measureList = new ArrayList<>(measureList.subList(overlap , measureList.size()));

            double meanValue = meanOfValue(window);
            long meanTime = meanOfTime(window);

            return new LocalStat(idHouse, meanValue, meanTime);
        }
        catch (IndexOutOfBoundsException e){
            return null;
        }
    }

    private long meanOfTime(ArrayList<Measurement> measureListForMean) {
        long sum = 0;

        for (Measurement m: measureListForMean){
            sum = sum + m.getTimestamp();
        }

        long ris = sum/windowSize;
        return ris;
    }

    private double meanOfValue(ArrayList<Measurement> measureListForMean) {
        double sum = 0;

        for (Measurement m: measureListForMean){
            sum = sum + m.getValue();
        }

        double ris = sum/windowSize;
        return ris;
    }

    public synchronized void forceWakeUp(){
        notify();
    }
}

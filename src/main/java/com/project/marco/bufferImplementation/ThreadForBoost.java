package com.project.marco.bufferImplementation;

import com.project.marco.houseInTheRing.Queue;
import com.project.marco.simulation_src_2019.SmartMeterSimulator;
import it.ewlab.ringMsg.RingMsgOuterClass.*;

public class ThreadForBoost extends Thread{

    private SmartMeterSimulator smartMeterSimulator;
    private Queue queue;
    private RingMsg msg;

    public ThreadForBoost(SmartMeterSimulator smartMeterSimulator, Queue queue, RingMsg msg) {
        this.smartMeterSimulator = smartMeterSimulator;
        this.queue = queue;
        this.msg = msg;
    }

    public void run() {
        try {
            System.out.println("Boost Started");
            smartMeterSimulator.boost();
            System.out.println("Boost Finished");
            queue.put(msg);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

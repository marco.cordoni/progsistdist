package com.project.marco.houseInTheRing;

import it.ewlab.ringMsg.RingMsgOuterClass.RingMsg;

import java.util.ArrayList;

public class Queue {
	private ArrayList<RingMsg> buffer = new ArrayList<>();
	
	public synchronized void put(RingMsg message) {
		buffer.add(message);
		notify();
	}

	public synchronized RingMsg take() {
		RingMsg message = null;

		while(buffer.size() == 0) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		if(buffer.size()>0){
			message = buffer.get(0);
			buffer.remove(0);
		}

		return message;
	}
}

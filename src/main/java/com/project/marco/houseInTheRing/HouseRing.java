package com.project.marco.houseInTheRing;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.project.marco.resourcesOnServerRest.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.util.ArrayList;

public class HouseRing {
    private static int id;
    private static InetAddress ip;
    private static int localServerPort;

    private static MultiServerHouse multiServerHouse;
    private static ClientHouse clientHouse;
    private static Queue queue;

    private static boolean isFirstHouse;
    private static boolean isActive;

    public static void main (String[] args){
        System.out.println("Hi, I'm a new house!");

        BufferedReader readInput = new BufferedReader(new InputStreamReader(System.in));
        isActive = true;

        try {
            System.out.println("Write an ID");
            id = getPositiveNumber(readInput);

            ip = InetAddress.getByName("localhost");

            System.out.println("Write a Port");
            localServerPort = getPositiveNumber(readInput);

            House thisHouse = new House(id, ip, localServerPort);
            ClientResponse response = registerOnServer(thisHouse);

            if (response.getStatus() == 406) {
                System.out.println("ERROR: THERE IS ALREADY A HOUSE WITH THIS ID");
            }
            else if (response.getStatus() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
            }
            else {
                setUpStructure(thisHouse, response);
                System.out.println("\n" +
                        "-------------------------------------------------------------------------------------------------\n" +
                        "Write:\n" +
                        "   b) to start the boost\n" +
                        "   e) to delete this house and exit from the ring\n" +
                        "-------------------------------------------------------------------------------------------------\n");
                System.out.println("Insert a function:\n");

                while (isActive){
                    String read = readInput.readLine();

                    if(read.equals("b")){
                        System.out.println("Waiting for boost....");

                        //Thread.sleep(4000); // per test

                        startBoost();
                    }
                    else if (read.equals("e")){
                        System.out.println("Start to exit....");

                        //Thread.sleep(4000); // per test

                        startExit();
                    }
                    else
                        System.out.println("ERROR: Command \"" + read + "\" not recognized!");
                }
            }
        }
        catch (ClientHandlerException e){
            System.out.println("Server not active!");
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("Goodbye!");
    }

    private static void setUpStructure(House thisHouse, ClientResponse response) throws JSONException, UnknownHostException {
        String output = response.getEntity(String.class);

        ArrayList<House> housesList = fromStringToHouseList(output);  // lista di case nel ring
        ArrayList<Integer> allId = fromHouseListToIdList(housesList); // lista di id delle case nel ring

        isFirstHouse = housesList.size() <= 1; // true se questa è l'unica casa nella rete e false altrimenti

        House nextHouse = getHouse(housesList);
        setUpServerAndClient(thisHouse, nextHouse, allId);

        multiServerHouse.start(); // AVVIO SERVER MULTI-THREAD
    }

    private static void startBoost() {
        clientHouse.boost();
    }

    private static void startExit() throws IOException {

        try{
            // MI CANCELLO DALLA LISTA DELLE CASE
            HttpURLConnection conn = deleteHouseFromServer();

            if (conn.getResponseCode() != 202)
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
        }
        catch (ConnectException e){
            System.out.println("Il server non è attivo e quindi non ho potuto cancellarmi dalla lista delle case");
        }

        //DICO AL CLIENT CHE VOGLIO INIZIARE LA PROCEDURA PER FERMARMI
        clientHouse.exitFromRing();
        isActive = false;
    }

    private static ArrayList<House> fromStringToHouseList(String output) throws JSONException, UnknownHostException {
        JSONArray array = new JSONArray(output);

        ArrayList<House> housesList = new ArrayList<>();

        for (int i = 0; i < array.length(); i++) {
            JSONObject object = array.getJSONObject(i);

            int id = Integer.parseInt(object.getString("id"));
            InetAddress ip = InetAddress.getByName(object.getString("ip"));
            int port = Integer.parseInt(object.getString("port"));

            House h = new House(id, ip, port);
            housesList.add(h);
        }
        return housesList;
    }

    private static ArrayList<Integer> fromHouseListToIdList(ArrayList<House> housesList) {
        ArrayList<Integer> list = new ArrayList<>();

        for (House h: housesList)
            list.add(h.getId());

        return list;
    }

    private static House getHouse(ArrayList<House> housesList) {
        House house = null;

        if (housesList.size() == 1)
            house = housesList.get(0);
        else
            for (House h: housesList) {
                if (h.getId() != id){
                    house = h;
                    break;
                }
            }
        return house;
    }

    private static void setUpServerAndClient(House thisHouse, House nextHouse, ArrayList<Integer> allId) {
        queue = new Queue();
        multiServerHouse = new MultiServerHouse(localServerPort, queue);
        clientHouse = new ClientHouse(thisHouse, nextHouse, multiServerHouse, isFirstHouse, queue, allId);
        multiServerHouse.setClientHouse(clientHouse);
    }

    private static HttpURLConnection deleteHouseFromServer() throws IOException {
        String urlToDelete = "http://localhost:1337/houses/remove/" + id;
        URL url = new URL(urlToDelete);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("DELETE");
        return conn;
    }

    private static ClientResponse registerOnServer(House thisHouse) {

        Client client = Client.create();
        WebResource webResource = client.resource("http://localhost:1337/houses/addHouse");

        String signToServer =
            "{\n" +
                "  \"id\": \""   + thisHouse.getId()                  + "\",\n" + // int
                "  \"ip\": \""   + thisHouse.getIp().getHostAddress() + "\",\n" + // inetAddress
                "  \"port\": \"" + thisHouse.getPort()                + "\"\n"  + // int
            "}";

        // SIGN UP TO THE SERVER
        return webResource.type("application/json").post(ClientResponse.class, signToServer);
    }

    private static int getPositiveNumber(BufferedReader readInput) throws IOException {
        int number;
        while (true){
            try {
                number = Integer.parseInt(readInput.readLine());

                if (number > 0 && number < 65535)
                    break;
                else
                    throw new NumberFormatException();
            }
            catch (NumberFormatException e){
                System.out.println("Please write a number > 0 and < 65535");
            }
        }
        return number;
    }
}

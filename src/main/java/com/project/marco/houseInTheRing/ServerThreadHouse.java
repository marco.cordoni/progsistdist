package com.project.marco.houseInTheRing;

import it.ewlab.ringMsg.RingMsgOuterClass.*;

import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;

public class ServerThreadHouse extends Thread{
    private Socket previousHouseSocket;
    private Queue queue;

    public ServerThreadHouse(Socket previousHouseSocket, Queue queue){
        this.previousHouseSocket = previousHouseSocket;
        this.queue = queue;
    }

    public void run(){
        RingMsg msg;

        try {
            while (true){
                msg = RingMsg.parseDelimitedFrom(previousHouseSocket.getInputStream());

                if (msg!=null && msg.getTypeOfMsg().equals(MsgType.DIETHREAD))
                    break;

                //Thread.sleep(1000);

                queue.put(msg); // PASSO LA RICHIESTA AL MIO CLIENT CHE LA PROPAGHERA' ALLA PROSSIMA CASA
            }
        }
        catch (SocketException e) {
            System.out.println("Thread connected with the previous house off!");
        }
        catch (IOException e){
            System.out.println("Thread connected with the previous house offf!");
            e.printStackTrace();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public void setInactive() throws IOException {
        previousHouseSocket.close();
    }
}


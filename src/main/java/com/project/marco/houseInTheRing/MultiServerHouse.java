package com.project.marco.houseInTheRing;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;

public class MultiServerHouse extends Thread{

    private int serverPort;
    private Queue queue;

    private ArrayList<ServerThreadHouse> threadList;
    private ServerSocket welcomeSocket;
    private ServerThreadHouse threadServer;
    private ClientHouse clientHouse;

    public MultiServerHouse(int serverPort, Queue queue){
        this.serverPort = serverPort;
        this.queue = queue;
    }

    public void run(){
        try {
            setUpStructure();
            clientHouse.start(); // avvio il client

            while (true){
                Socket previousHouseSocket = welcomeSocket.accept(); // accetto un client

                threadServer = new ServerThreadHouse(previousHouseSocket, queue); // instanzio thread che si occupa di quel client
                threadServer.start(); // faccio partire il thread

                threadList.add(threadServer);
            }
        } catch (SocketException e) {
            System.out.println("Server off");
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setUpStructure() throws IOException {
        threadList = new ArrayList<>();
        welcomeSocket = new ServerSocket(serverPort); // creo server sulla porta passata
    }

    public void setInactive() throws IOException {
        for (ServerThreadHouse s: threadList) {
            if (s.isAlive())
                s.setInactive();
        }
        welcomeSocket.close();
    }

    public void setClientHouse(ClientHouse clientHouse){
        this.clientHouse = clientHouse;
    }
}

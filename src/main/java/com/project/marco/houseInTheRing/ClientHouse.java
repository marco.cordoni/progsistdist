package com.project.marco.houseInTheRing;

import com.project.marco.bufferImplementation.LocalStatisticManager;
import com.project.marco.globalStatistic.GlobalStatisticManager;
import com.project.marco.resourcesOnServerRest.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.util.ArrayList;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import it.ewlab.ringMsg.RingMsgOuterClass;
import it.ewlab.ringMsg.RingMsgOuterClass.*;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public class ClientHouse extends Thread{
    private MultiServerHouse multiServerHouse;
    private House nextHouse;
    private boolean isFirstHouse;
    private Queue queue;
    private House thisHouse;
    private final ArrayList<Integer> idList;

    private Socket nextHouseSocket;
    private boolean enterInTheRing;
    private boolean inExit;
    private boolean isCoordinator;
    private LocalStatisticManager localStatisticManager;
    private boolean wannaBoost;
    private GlobalStatisticManager globalStatisticManager;

    public ClientHouse(House thisHouse, House nextHouse, MultiServerHouse multiServerHouse, boolean isFirstHouse, Queue queue, ArrayList<Integer> allId){
        this.thisHouse = thisHouse;
        this.nextHouse = nextHouse;
        this.multiServerHouse = multiServerHouse;
        this.isFirstHouse = isFirstHouse;
        this.queue = queue;
        this.idList = allId;

        this.enterInTheRing = !isFirstHouse;
        this.inExit = false;
        this.isCoordinator = isFirstHouse;
        this.wannaBoost = false;
    }

    public void run() {
        RingMsg msg;

        try {
            nextHouseSocket = new Socket(nextHouse.getIp(), nextHouse.getPort()); // creo socket con la prossima casa del ring
            localStatisticManager = new LocalStatisticManager(thisHouse.getId(), queue);
            globalStatisticManager = new GlobalStatisticManager(idList);

            System.out.println("I'm: " + thisHouse.getId() + " and I'm connected to: " + nextHouse.getId());

            manageEnterInTheRing();

            while (true){
                msg = queue.take();
                MsgType typeOfMsg = msg.getTypeOfMsg();

                //Thread.sleep(1500);
                //if (typeOfMsg.equals(MsgType.TOKENBOOST)) System.out.println("CLIENT: io sono -> " + thisHouse.getId() + "; invio a " + nextHouse.getId() + "; ho  ricevuto -> " + msg.toString());

                // AGGIORNO LA LISTA DI CASE PER POTER CALCOLARE LE STATISTICHE GLOBALI SE IL MESSAGGIO IN ARRIVO E DI ENTRATA O USCITA DI UNA CASA NELLA RETE
                updateIdList(msg, typeOfMsg);

                // SE RICEVO UN TOKEN E VOGLIO USARE IL BOOST ALLORA ATTIVO IL BOOST E MI SEGNO COME SE NON VOGLIO PIU' USARLO ALL'ARRIVO DEL PROSSIMO TOKEN
                if (typeOfMsg.equals(MsgType.TOKENBOOST) && wannaBoost){
                    manageBoost(msg);
                }

                // SE RICEVO UN MESSAGGIO DI INGRESSO DI UNA NUOVA CASA CHE SI VUOLE METTERE TRA ME ED IL MIO SUCCESSORE LEI DIVENTERA' IL MIO NUOVO SUCCESSORE
                else if (typeOfMsg.equals(MsgType.NEWHOUSE) && msg.getNextHouse().getId() == nextHouse.getId()){
                    manageConnectToTheNewHouse(msg);
                }

                // SE RICEVO UN MESSAGGIO DI INGRESSO DI UNA NUOVA CASA CHE SI VUOLE METTERE PRIMA DI ME MA IO STO USCENDO DALLA RETE MODIFICO IL SUO SUCCESSORE CON IL MIO SUCCESSORE
                else if (typeOfMsg.equals(MsgType.NEWHOUSE) && msg.getNextHouse().getId() == thisHouse.getId() && inExit){
                    manageChangeConnectionWhenExiting(msg);
                }

                // SE RICEVO UN MESSAGGIO DI CONNECT SIGNIFICA CHE POSSO ENTRARE NELLA RETE COLLEGANDOMI ALLA CASA NEL MESSAGGIO CHE POTREBBE NON ESSERE QUELLA A CUI HO FATTO RICHIESTA ALL'INIZIO PERCHE' POTREBBE ESSERE USCITA
                else if(typeOfMsg.equals(MsgType.CONNECT)){
                    manageConnectToTheRing(msg);
                }

                // SE RICEVO UN MESSAGGIO DI EXIT E LA CASA CHE SI VUOLE TOGLIERE E' LA MIA PROSSIMA ALLORA LE MANDO UN MESSAGGIO CHE DICE CHE PUO' MORIRE E IO MI COLLEGO AL SUO SUCCESSORE
                else if(typeOfMsg.equals(MsgType.EXITHOUSE) && msg.getNewOrExitHouse().getId() == nextHouse.getId()){
                    manageExitHouseAndMakeNewConnection(msg);
                }

                // SE RICEVO UN MESSAGGIO DI EXIT E LA CASA CHE DOVRA' COLLEGARSI AD UNA SUCCESSIVA SI DIVE COLLEGARE A ME MA IO VOLEVO USCIRE DALLA RETE ALLORA LA FACCIO CONNETTERE AL MIO SUCCESSORE
                else if(typeOfMsg.equals(MsgType.EXITHOUSE) && msg.getNextHouse().getId() == thisHouse.getId() && inExit){
                    manageExitHouseWhenExiting(msg);
                }

                // SE RICEVO UN MESSAGGIO DI DIEHOUSE SIGNIFICA CHE LA MIA CASA PRECEDENTE E' STATA INFORMATA DEL FATTO CHE STO USCENDO DALLA RETE E QUINDI POSSO MORIRE
                else if (typeOfMsg.equals(MsgType.DIEHOUSE)){
                    manageDieThisHouse();
                    break;
                }

                // SE RICEVO UN MESSAGGIO DI ELEZIONE E NON VOLEVO USCIRE DALLA RETE ALLORA LO CONTROLLO PER VEDERE SE POSSO DIVENTARE COORDINATORE
                else if (typeOfMsg.equals(MsgType.ELECTION) && !inExit){
                    manageElection(msg);
                }

                // SE RICEVO UN MESSAGGIO DI ELEZIONE E IO SONO QUELLO CHE DOVREBBE ESSERE ELETTO MA STAVO PER USCIRE DALLA RETE ALLORA INDICO UNA NUOVA ELEZIONE
                else if (typeOfMsg.equals(MsgType.ELECTION) && inExit && msg.getElectionId() == thisHouse.getId()){
                    sendElection();
                }

                // SE SONO IL COORDINATORE NON REINOLTRO LE STATISTICHE LOCALI E LE AGGIUNGO AD UNA LISTA CHE LA USO PER CALCOLARE LE STATISTICHE GLOBALI
                else if (typeOfMsg.equals(MsgType.LOCALSTAT) && isCoordinator){
                    manageLocalStat(msg);
                }

                // SE RICEVO UNA STATISTICA GLOBALE...
                else if(typeOfMsg.equals(MsgType.GLOBALSTAT)){
                    manageGlobalStat(msg);
                }

                // SE NON E' NESSUNO DEI CASI PRECEDENTI PROPAGO SOLO IL MESSAGGIO
                else{
                    msg.writeDelimitedTo(nextHouseSocket.getOutputStream());
                }
            }
        }
        catch (IOException e){
            System.out.println("Client off!");
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    private void manageEnterInTheRing() throws IOException, JSONException {
        if (isFirstHouse){ // SE SONO L'UNICA CASA NELLA RETE CREO I TOKEN PER IL BOOST
            sendTokenForBoostTo(nextHouseSocket);
            localStatisticManager.start();
            isFirstHouse = false;
        }

        if(enterInTheRing){ // SE NON SONO LA PRIMA CASA NELLA RETE MANDO LA RICHIESTA PER ENTRARE NELLA RETE IN MODO TALE CHE IL MIO PREDECESSORE MI CONOSCA
            boolean conenctionRetry = true;

            while (conenctionRetry){
                try {

                    //Thread.sleep(1000 * 10); // per test su ingresso se la casa a cui mi voglio collegare muore

                    sendNewHouse(nextHouseSocket);
                    enterInTheRing = false;
                    conenctionRetry = false;

                    // PRENDO LA LISTA DI CASE DAL SERVER
                    ArrayList<House> houseList = getHouses();

                    // AGGIORNO LA LISTADI ID PER CALCOLARE LA GLOBAL STAT PERCHE' QUALCHE CASA POTREBBE ESSERE USCITA NEL FRATTEMPO
                    ArrayList<Integer> newIdList = fromHouseListToIdList(houseList);
                    globalStatisticManager.updateIdLIst(newIdList);

                } catch (SocketException e){ // SE ALZA QUESTA ECCEZIONE SIGNIFICA CHE LA CASA A CUI DOVEVO FARE RICHIESTA DI INGRESSO E' MORTA NEL FRATTEMPO
                    //System.out.println("La casa a cui volevo collegarmi è morta, ne prendo una nuova");

                    // PRENDO UNA NUOVA CASA DAL SERVER REST
                    ArrayList<House> houseList = getHouses();
                    House newNextHouse = getHouse(houseList, thisHouse.getId());

                    // AGGIORNO LA LISTADI ID PER CALCOLARE LA GLOBAL STAT
                    ArrayList<Integer> newIdList = fromHouseListToIdList(houseList);
                    globalStatisticManager.updateIdLIst(newIdList);

                    // LA METTO COME nextHouse
                    nextHouse = newNextHouse;

                    // CREO UNA NUOVA nextHouseSocket
                    nextHouseSocket = new Socket(nextHouse.getIp(), nextHouse.getPort()); // creo socket con la prossima casa del ring

                    // SE LA NUOVA CASA E' ME ALLORA DEVO DIVENTARE COORDINATORE E RICREARE I TOKEN
                    if(thisHouse.getId() == nextHouse.getId()){
                        System.out.println("I'M THE NEW COORDINATOR");
                        isCoordinator = true;
                        sendTokenForBoostTo(nextHouseSocket);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            //System.out.println("Connessione riuscita con: " + nextHouse.toString() + " con socket " + nextHouseSocket.toString());
        }
    }

    private void manageGlobalStat(RingMsg msg) throws IOException {
        if (isCoordinator){ // ...E SONO COORDINATORE LA STAMPO A VIDEO E NON LA INOLTRO AL PROSSIMO PERCHE' SIGNIFICA CHE HA GIA' FATTO UN GIRO COMPLETO
            // LA STAMPO A VIDEO
            System.out.println("GLOBAL CONSUMPTION: " + msg.getGlobalStat().getValue() + "; AT THE TIME: " + msg.getGlobalStat().getTimestamp());

        }
        else { // SE NON SONO COORDINATORE LA INOLTRO AL PROSSIMO PERCHE' NON SONO ANCORA STATI INFORMATI TUTTI
            // LA STAMPO A VIDEO
            System.out.println("GLOBAL CONSUMPTION: " + msg.getGlobalStat().getValue() + "; AT THE TIME: " + msg.getGlobalStat().getTimestamp());

            // POI LA REINOLTRO NELLA RETE
            msg.writeDelimitedTo(nextHouseSocket.getOutputStream());
        }
    }

    private void manageLocalStat(RingMsg msg) throws IOException {
        int id = msg.getLocalStat().getId();
        double value = msg.getLocalStat().getValue();
        long timestamp = msg.getLocalStat().getTimestamp();

        LocalStat newLocalStat = new LocalStat(id, value, timestamp);

        globalStatisticManager.addLocalStat(newLocalStat);

        // CALCOLO LA STISTICA GLOBALE
        GlobalStat newGlobalStat = globalStatisticManager.calcGlobalStatIfPossible();

        // SE NON RITORNA NULL E QUINDI HO POTUTO CALCOLARLA
        if (newGlobalStat != null){

            // LA INVIO AL SERVER
            Client client = Client.create();
            WebResource webResource = client.resource("http://localhost:1337/houses/addGlobalStat");

            String toServer =
                    "{\n" +
                            "  \"value\": \""     + newGlobalStat.getValue()     + "\",\n" + // double
                            "  \"timestamp\": \"" + newGlobalStat.getTimestamp() + "\"\n"  + // long
                    "}";

            // INVIO STATISTICHE GLOBALI AL SERVER
            try{
                ClientResponse response = webResource.type("application/json").post(ClientResponse.class, toServer);

                if (response.getStatus() != 200) {
                    throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
                }
            }
            catch (ClientHandlerException e){
                System.out.println("Il server non è attivo e quindi non ho potuto inviare le statistiche globali");
            }

            // LA PASSO AL MIO SUCCESSORE
            RingMsg msgGlobalStat = RingMsg.newBuilder()
                    .setTypeOfMsg(MsgType.GLOBALSTAT)
                    .setGlobalStat(GlobalStatProto.newBuilder()
                            .setValue(newGlobalStat.getValue())
                            .setTimestamp(newGlobalStat.getTimestamp()))
                    .build();

            msgGlobalStat.writeDelimitedTo(nextHouseSocket.getOutputStream());
        }
    }

    private void manageElection(RingMsg msg) throws IOException {
        int electionNumber = msg.getElectionId();

        if(electionNumber < thisHouse.getId()){
            RingMsg msgElection = RingMsg.newBuilder()
                    .setTypeOfMsg(MsgType.ELECTION)
                    .setElectionId(thisHouse.getId())
                    .build();

            msgElection.writeDelimitedTo(nextHouseSocket.getOutputStream());
        }
        else if (electionNumber == thisHouse.getId()){
            isCoordinator = true;
            System.out.println("I'M THE NEW COORDINATOR");
        }
        else
            msg.writeDelimitedTo(nextHouseSocket.getOutputStream());
    }

    private void manageDieThisHouse() throws IOException {
        sendDieThread(nextHouseSocket);
        multiServerHouse.setInactive();
        System.out.println("Client off!");
    }

    private void manageExitHouseWhenExiting(RingMsg msg) throws IOException {
        RingMsg msgExit = RingMsg.newBuilder()
                .setTypeOfMsg(MsgType.EXITHOUSE)
                .setNewOrExitHouse(HouseProto.newBuilder()
                        .setId(msg.getNewOrExitHouse().getId())
                        .setIp(msg.getNewOrExitHouse().getIp())
                        .setPort(msg.getNewOrExitHouse().getPort()))
                .setNextHouse(HouseProto.newBuilder()
                        .setId(nextHouse.getId())
                        .setIp(nextHouse.getIp().toString())
                        .setPort(nextHouse.getPort()))
                .build();

        msgExit.writeDelimitedTo(nextHouseSocket.getOutputStream());
    }

    private void manageExitHouseAndMakeNewConnection(RingMsg msg) throws IOException {
        // DICO AL VECCHIO SUCCESSORE CHE PUO MORIRE
        RingMsg msgDieHouse = RingMsg.newBuilder()
                .setTypeOfMsg(MsgType.DIEHOUSE)
                .build();

        msgDieHouse.writeDelimitedTo(nextHouseSocket.getOutputStream());
        nextHouseSocket.close();

        // MI COLLEGO A QUELLO NUOVO
        changeNextHouse(msg);

        System.out.println("I'm: " + thisHouse.getId() + " and I'm connected to: " + nextHouse.getId());
    }

    private void manageConnectToTheRing(RingMsg msg) throws IOException {
        // MI SCOLLEGO DA QUELLO A CUI ERO COLLEGATO
        sendDieThread(nextHouseSocket);
        nextHouseSocket.close();

        // MI COLLEGO A QUELLO INDICATO NEL MESSAGGIO
        changeNextHouse(msg);
        localStatisticManager.start();

        System.out.println("I'm: " + thisHouse.getId() + " and I'm connected to: " + nextHouse.getId());
    }

    private void manageChangeConnectionWhenExiting(RingMsg msg) throws IOException {
        RingMsg msgNew = RingMsg.newBuilder()
                .setTypeOfMsg(MsgType.NEWHOUSE)
                .setNewOrExitHouse(HouseProto.newBuilder()
                        .setId(msg.getNewOrExitHouse().getId())
                        .setIp(msg.getNewOrExitHouse().getIp())
                        .setPort(msg.getNewOrExitHouse().getPort()))
                .setNextHouse(HouseProto.newBuilder()
                        .setId(nextHouse.getId())
                        .setIp(nextHouse.getIp().toString())
                        .setPort(nextHouse.getPort()))
                .build();

        msgNew.writeDelimitedTo(nextHouseSocket.getOutputStream());
    }

    private void manageConnectToTheNewHouse(RingMsg msg) throws IOException {
        // DICO AL MIO VECCHIO SUCCESSORE CHE NON SONO PIU COLLEGATO A LUI
        sendDieThread(nextHouseSocket);
        nextHouseSocket.close(); // oldNext

        // PRENDO I DATI DEL NUOVO SUCCESSORE
        int newId = msg.getNewOrExitHouse().getId();
        InetAddress newIp = InetAddress.getByName("localhost");
        int newPort = msg.getNewOrExitHouse().getPort();

        // DEFINISCO IL NUOVO SUCCESSORE
        nextHouse = new House(newId, newIp, newPort);
        nextHouseSocket = new Socket(nextHouse.getIp(), nextHouse.getPort()); // creo socket con la prossima casa del ring

        //DICO AL MIO SUCCESSORE CHE E' APPENA ENTRATO A CHI SI DEVE COLLEGARE PERCHE' QUELLO A CUI SI VOLEVA COLLEGARE POTREBBE ESSERE MORTO NEL FRATTEMPO
        RingMsg msgConnect = RingMsg.newBuilder()
                .setTypeOfMsg(MsgType.CONNECT)
                .setNextHouse(HouseProto.newBuilder()
                        .setId(msg.getNextHouse().getId())
                        .setIp(msg.getNextHouse().getIp())
                        .setPort(msg.getNextHouse().getPort()))
                .build();

        msgConnect.writeDelimitedTo(nextHouseSocket.getOutputStream());

        System.out.println("I'm: " + thisHouse.getId() + " and I'm connected to: " + nextHouse.getId());
    }

    private void manageBoost(RingMsg msg) {
        localStatisticManager.boost(msg);
        wannaBoost = false;
    }

    private void updateIdList(RingMsg msg, MsgType typeOfMsg) {
        // SE IL MESSAGGIO CHE ARRIVA E' DI ENTRATA...
        if(typeOfMsg.equals(MsgType.NEWHOUSE)){
            int newHouseId = msg.getNewOrExitHouse().getId();
            globalStatisticManager.addId(newHouseId);
        }
        else if(typeOfMsg.equals(MsgType.EXITHOUSE)){ // ...O USCITA DI UNA CASA DALLA RETE AGGIORNO LA LISTA DEGLI ID PER POTER GENERARE STATISTICHE GLOBALI
            int exitHouseId = msg.getNewOrExitHouse().getId();
            globalStatisticManager.removeId(exitHouseId);
        }
    }

    private void sendElection() throws IOException {
        RingMsg msgElection = RingMsg.newBuilder()
                .setTypeOfMsg(MsgType.ELECTION)
                .setElectionId(-1)
                .build();

        queue.put(msgElection);
    }

    private void changeNextHouse(RingMsg msg) throws IOException {
        int newId = msg.getNextHouse().getId();
        InetAddress newIp = InetAddress.getByName("localhost");
        int newPort = msg.getNextHouse().getPort();

        nextHouse = new House(newId, newIp, newPort);
        nextHouseSocket = new Socket(nextHouse.getIp(), nextHouse.getPort()); // creo socket con la prossima casa del ring
    }

    private void sendDieThread(Socket socketMustDie) throws IOException {
        RingMsg msgDieThread = RingMsg.newBuilder()
                .setTypeOfMsg(MsgType.DIETHREAD)
                .build();

        msgDieThread.writeDelimitedTo(socketMustDie.getOutputStream());
    }

    private void sendNewHouse(Socket nextHouseSocket) throws IOException {
        RingMsg msgNew = RingMsg.newBuilder()
                .setTypeOfMsg(MsgType.NEWHOUSE)
                .setNewOrExitHouse(HouseProto.newBuilder()
                    .setId(thisHouse.getId())
                    .setIp(thisHouse.getIp().toString())
                    .setPort(thisHouse.getPort()))
                .setNextHouse(HouseProto.newBuilder()
                        .setId(nextHouse.getId())
                        .setIp(nextHouse.getIp().toString())
                        .setPort(nextHouse.getPort()))
                .build();

        msgNew.writeDelimitedTo(nextHouseSocket.getOutputStream());

        //System.out.println("Ho scritto un messaggio a: " + nextHouse.toString() + "; " + nextHouseSocket.toString());
    }

    private void sendTokenForBoostTo(Socket nextHouse) throws IOException {
        RingMsg msgToken1 = RingMsg.newBuilder()
                .setTypeOfMsg(MsgType.TOKENBOOST)
                .setTokenNumber("TOKEN - 1")
                .build();

        RingMsg msgToken2 = RingMsg.newBuilder()
                .setTypeOfMsg(MsgType.TOKENBOOST)
                .setTokenNumber("TOKEN - 2")
                .build();

        msgToken1.writeDelimitedTo(nextHouse.getOutputStream());
        msgToken2.writeDelimitedTo(nextHouse.getOutputStream());
    }

    private void setInactive() throws IOException {
        multiServerHouse.setInactive();
        nextHouseSocket.close();
        queue.put(RingMsg.newBuilder().setTypeOfMsg(MsgType.EXITHOUSE).build());
    }

    public void exitFromRing() throws IOException {
        try {
            localStatisticManager.threadForBoost.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        catch (NullPointerException e){

        }

        inExit = true;

        if (thisHouse.getId() == nextHouse.getId()){
            localStatisticManager.stopLocalStatisticManager();
            setInactive();
        }
        else {
            localStatisticManager.stopLocalStatisticManager();

            RingMsg msgExit = RingMsg.newBuilder()
                    .setTypeOfMsg(MsgType.EXITHOUSE)
                    .setNewOrExitHouse(HouseProto.newBuilder()
                            .setId(thisHouse.getId())
                            .setIp(thisHouse.getIp().toString())
                            .setPort(thisHouse.getPort()))
                    .setNextHouse(HouseProto.newBuilder()
                            .setId(nextHouse.getId())
                            .setIp(nextHouse.getIp().toString())
                            .setPort(nextHouse.getPort()))
                    .build();

            queue.put(msgExit);

            if(isCoordinator){
                sendElection();
            }

            // try { Thread.sleep(1000 * 10); } catch (InterruptedException e) { e.printStackTrace(); }

        }
    }

    public void boost() {
        this.wannaBoost = true;
    }

    private static ArrayList<House> getHouses() throws IOException, JSONException {
        String urlToGet = "http://localhost:1337/houses/getHouses";
        HttpURLConnection conn = getConnection(urlToGet);
        JSONArray array = getJsonArray(conn);

        ArrayList<House> housesList = new ArrayList<>();

        for (int i = 0; i < array.length(); i++) {
            JSONObject object = array.getJSONObject(i);

            int id = Integer.parseInt(object.getString("id"));
            InetAddress ip = InetAddress.getByName(object.getString("ip"));
            int port = Integer.parseInt(object.getString("port"));

            House h = new House(id, ip, port);
            housesList.add(h);
        }
        conn.disconnect();
        return housesList;
    }

    private static HttpURLConnection getConnection(String urlToGet) throws IOException {
        URL url = new URL(urlToGet);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Accept", "application/json");

        if (conn.getResponseCode() != 200)
            throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
        return conn;
    }

    private static JSONArray getJsonArray(HttpURLConnection conn) throws IOException, JSONException {
        BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
        String output = br.readLine();
        return new JSONArray(output);
    }

    private static House getHouse(ArrayList<House> housesList, int myId) {
        House house = null;

        if (housesList.size() == 1)
            house = housesList.get(0);
        else
            for (House h: housesList) {
                if (h.getId() != myId){
                    house = h;
                    break;
                }
            }
        return house;
    }

    private static ArrayList<Integer> fromHouseListToIdList(ArrayList<House> housesList) {
        ArrayList<Integer> list = new ArrayList<>();

        for (House h: housesList)
            list.add(h.getId());

        return list;
    }
}
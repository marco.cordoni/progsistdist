package com.project.marco.resourcesOnServerRest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class LocalStat {

    private int id;
    private double value;
    private long timestamp;

    public LocalStat(){} // costruttore vuoto

    public LocalStat(int id, double value, long timestamp) {
        this.id = id;
        this.value = value;
        this.timestamp = timestamp;
    }

    public int getId() { return id; }

    public void setId(int id) {
        this.id = id;
    }

    public double getValue() { return value; }

    public void setValue(double value) {
        this.value = value;
    }

    public long getTimestamp() {
        return this.timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String toString(){
        return "ID: " + id + "; Value: " + value + "; Timestamp: " + timestamp;
    }
}

package com.project.marco.resourcesOnServerRest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class GlobalStat {

    private double value;
    private long timestamp;

    public GlobalStat(){} // costruttore vuoto

    public GlobalStat(double value, long timestamp) {
        this.value = value;
        this.timestamp = timestamp;
    }

    public double getValue() { return value; }

    public void setValue(double value) {
        this.value = value;
    }

    public long getTimestamp() {
        return this.timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String toString(){
        return "Value: " + value + "; Timestamp: " + timestamp;
    }
}

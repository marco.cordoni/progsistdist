package com.project.marco.resourcesOnServerRest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;

@XmlRootElement
@XmlAccessorType (XmlAccessType.FIELD) // per dire a JAXB che può accedere internamente in una struttura dati
public class Houses {

    @XmlElement(name="houses") // in fase di serializzazione in json cambio il nome in "houses", questo sarà il nome mostrato della lista che appare con "http://localhost:1337/houses"
    private ArrayList<House> housesList;

    @XmlElement(name="globalStats")
    private ArrayList<GlobalStat> globalStatList;

    @XmlElement(name="localStats")
    private ArrayList<LocalStat> localStatList;

    private static Houses instance;

    private Houses() {
        housesList = new ArrayList<House>();
        globalStatList = new ArrayList<GlobalStat>();
        localStatList = new ArrayList<LocalStat>();
    }

    //singleton
    public synchronized static Houses getInstance(){ /* uso un singleton perchè ogni richiesta in rest è gestita da un pool di thread concorrenti che accedono alla stessa struttura dati,
    ad ogni chiamata http corrisponde una nuova istanza di HouseService, così facendo con un singleton permetto che tutte le chiamata abbiamo la stessa vista sui dati, non sincronizzare HouseService*/
        if(instance==null)
            instance = new Houses();
        return instance;
    }

    public synchronized ArrayList<House> getHouseslist() { /* perchè è sincronizzato? per evitare che la lista venga modificata durante la copia (non è un'operazione atomica) e quindi si creino cose strane
    perchè creo una copia? perchè il metodo ritornerebbe altrimenti un puntatore alla lista la quale diverrebbe modificabile senza la protezione dall'accesso concorrente di più thread, esporrei una risorsa importante*/
        return new ArrayList<>(housesList);
    }

    public synchronized void addHouseToList(House h){
        housesList.add(h);
    }

    public ArrayList<House> addHouse(House house){
        ArrayList<House> housesCopy = getHouseslist();

        for(House h: housesCopy)
            if(h.getId() == house.getId()) // se trovo una casa con lo stesso identificatore ritorno null per indicare che la richiesta non è andata a buon fine
                return null;

        addHouseToList(house);
        return getHouseslist();
    }

    public synchronized void remove(int idToRemove){
        for(House h: housesList)
            if(h.getId() == idToRemove) {
                housesList.remove(h);
                return;
            }
    }

    public synchronized void addGlobalStat(GlobalStat globalStat){
        globalStatList.add(globalStat);
    }

    public synchronized ArrayList<GlobalStat> getGlobalStatList(){
        return new ArrayList<>(globalStatList);
    }

    public synchronized void addLocallStat(LocalStat localStat){
        localStatList.add(localStat);
    }

    public synchronized ArrayList<LocalStat> getLocalStatList(){
        return new ArrayList<>(localStatList);
    }
}
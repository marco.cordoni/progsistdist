package com.project.marco.resourcesOnServerRest;

import javax.xml.bind.annotation.XmlRootElement;
import java.net.InetAddress;

@XmlRootElement
public class House {

    private int id;
    private InetAddress ip;
    private int port;

    public House(){} // costruttore vuoto

    public House(int id, InetAddress ip, int port) {
        this.id = id;
        this.ip = ip;
        this.port = port;
    }

    public int getId() { return id; } // usare camel case, cioè ciaoComeStai con i nomi dei campi

    public void setId(int id) {
        this.id = id;
    }

    public InetAddress getIp() {
        return this.ip;
    }

    public void setIp(InetAddress ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String toString(){
        return "ID: " + id + "; IP: " + ip.toString() + "; PORT: " + port;
    }
}
